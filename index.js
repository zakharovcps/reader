const { connect, StringCodec, Empty, headers } = require("nats");
const fs = require('fs').promises;
const path = require('path');

const WebSocketClient = require('websocket').client;
const WebSocketConnection = require('websocket').connection;

// create an encoder
const sc = StringCodec();

const serverInfo = { server: "localhost", port: 4222, name: 'reader' }; //, pingInterval: 10000, maxPingOut: 2, },
const srcFilePath = `./files/file.jpg`;
const chunkSize = 0.8 * 1024 * 1024; // размер сообщения в байтах (блок считываемых данных)
const transport = ['nats', 'websocket'][0];

const log = function () {
    const args = [new Date().toJSON().split('T')[1].split('.')[0]]; // только время
    for (let i = 0; i < arguments.length; i++) {
        args.push(arguments[i]);
    }
    console.log.apply(console, args);
};

// websocket
if (transport == 'websocket') {
    (async () => {
        const client = new WebSocketClient();

        client.on('connectFailed', function(error) {
            log('Connect Error: ' + error.toString());
        });

        client.on('connect', function(connection) {
            log('WebSocket Client Connected');
            // текущий шаг
            // 0 - старт отправки
            // 1 - отправка данных файла
            // 2 - завершение
            // 3 - финиш
            let step = 0;
            let chunkCount = 0; // отправлено кусков на запись
            let chunkAcks = 0; // ответов на куски от сервера
            connection.on('error', function(error) {
                log("Connection Error: " + error.toString());
            });
            connection.on('close', function() {
                log('echo-protocol Connection Closed');
            });
            const closeConnection = (err) => {
                if (err) {
                    log(err);
                    connection.close(WebSocketConnection.CLOSE_REASON_INVALID_DATA, err.message);
                } else {
                    connection.close(WebSocketConnection.CLOSE_REASON_NORMAL, 'Вежливо завершаем');
                }
            };
            connection.on('message', function(message) {
                // ответы от сервера
                if (step == 0) {
                    if (message.type === 'utf8') {
                        if (message.utf8Data == 'ok') {
                            // подтверждено открытие файла
                            // начинаем отправку
                            step = 1;
                            return startSendFile();
                        } else {
                            return closeConnection(new Error(message.utf8Data));
                        }
                    }
                } else if (step == 1) {
                    if (message.type === 'utf8') {
                        if (message.utf8Data == 'ok') {
                            // подтверждено получение блока
                            // отправляем следующий
                            chunkAcks++;
                            return sendNextFileBlock();
                        } else {
                            return closeConnection(new Error(message.utf8Data));
                        }
                    }
                } else if (step == 2) {
                    if (message.type === 'utf8') {
                        if (message.utf8Data == 'ok') {
                            // подтверждено завершение
                            step = 3;
                            return closeConnection();
                        } else {
                            return closeConnection(new Error(message.utf8Data));
                        }
                    }
                }
                log(`Сообщение не поддерживается: ` + ((message.type === 'utf8') ? message.utf8Data : message.binaryData?.length));
                closeConnection('Сообщение не поддерживается');
            });

            let fh;
            let isFileDone = false;
            let totalRead = 0;
            const threadCount = 2;

            // запускает чтение файла в несколько потоков для их отправки
            function startSendFile () {
                // отправляем в N потоков с задержкой 100 мс
                let i = threadCount;
                while (i--) {
                    setTimeout(sendNextFileBlock, 100);
                }
            }

            // читает блок файла и отправляет
            function sendNextFileBlock() {
                if (isFileDone) {
                    return;
                }
                const buf = Buffer.allocUnsafe(chunkSize);
                
                fh.read(buf, 0, chunkSize, null)
                .then(async ({bytesRead}) => {
                    if (!bytesRead) {
                        step = 2;
                        // завершаем чтение файла
                        // если завершена отправка всех блоков
                        if (chunkAcks == chunkCount) {
                            return sendAction({
                                action: 'close',
                            });
                        }
                    }
                    log(`P: отправка байт: ${totalRead} - ${totalRead + bytesRead}`);
                    let metaBuf = Buffer.allocUnsafe(4);
                    metaBuf.writeUInt32LE(totalRead, 0);
                    let data = Buffer.concat([metaBuf, buf.slice(0, bytesRead)]);
                    totalRead += bytesRead;
                    // отправка
                    chunkCount++;
                    sendData(data);
                }).catch(err => closeConnection(err));
            }

            function sendAction(rec) {
                if (connection.connected) {
                    connection.sendUTF(JSON.stringify(rec));
                }
            }

            function sendData(buf) {
                if (connection.connected) {
                    connection.sendBytes(buf);
                }
            }

            // открываем файл на чтение
            fs.open(srcFilePath, 'r')
            .then(handler => {
                fh = handler;
                // начинаем сценарий отправкой команды на открытие файла на запись
                log(`Отправляем сообщение для открытия файла`);
                sendAction({
                    action: 'open',
                    fileName: path.basename(srcFilePath),
                    fileType: 'image/jpg',
                });
            }).catch(err => closeConnection(err));
        });

        client.connect('ws://localhost:8080/', 'echo-protocol');
    })().then(async () => {
        
    }).catch(async err => {
        log(err);
    });
} else if (transport == 'nats') {
    // nats
    (async () => {
        // подключаемся к nats
        const nc = await connect(serverInfo);

        // отправка файла по частям (читатель)
        let fh;
        try {
            fh = await fs.open(srcFilePath, 'r');
            let h = headers();
            h.append('Content-Type', 'image/jpeg'); // тип файла
            h.append('Name', path.basename(srcFilePath)); // имя файла
            // первое сообщение инициирует отправку
            log(`P: отправка файла ${srcFilePath}`);
            let res = await nc.request("writer.open", Empty, { timeout: 10000, headers: h });
            // ошибкой считается наличие данных в ответе
            if (res.data?.length) {
                throw new Error(sc.decode(res.data));
            }
            // последовательная отправка частей файла
            const buf = Buffer.allocUnsafe(chunkSize);
            let bytesRead, totalRead = 0;
            while (({ bytesRead } = await fh.read(buf, 0, chunkSize, null)) && bytesRead) {
                // отправка чанка файла
                h = headers();
                h.append('Offset', totalRead.toString()); // в какое место файла писать
                log(`P: отправка байт: ${totalRead} - ${totalRead + bytesRead}`);
                res = await nc.request("writer.write", buf.slice(0, bytesRead), { timeout: 10000, headers: h });
                if (res.data?.length) {
                    throw new Error(sc.decode(res.data));
                }
                log(`R: подтверждена отправка байт: ${bytesRead}`);
                totalRead += bytesRead;
            }
            // завершаем отправку / закрываем файл
            res = await nc.request("writer.close", Empty);
        } catch (e) {
            if (e.code == 503) {
                log('P: Ошибка запроса: нет получателя');
            } else {
                log(`P: Ошибка запроса`, e);
            }
        } finally {
            if (fh) {
                await fh.close();
            }
        }

        return nc;
    })().then(async nc => {
        let err = await nc.drain();
        log(err || 'отключение (drain), ридер отправил файл');
    }).catch(async err => {
        log(err);
        try {
            await nc.close();
        } catch (err) { }
    });
}


